# Test Runner
This project contains the test suite, this includes all of the tests which will be run remotely via HTTP calls
## Usage
Use [docker-compose](https://docs.docker.com/compose/) to run the application manually. 
```bash 
$ docker-compose up --build 
```
