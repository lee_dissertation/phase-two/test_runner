import json
import os
import time
import requests
import unittest

from .messages import *

"""
TestBuilder manages building a single test, which features a number of steps to execute
"""
class TestBuilder():

	def __init__(self):
 		self.tests=[]

	def addSend(self,testName,url,requestFilePath,responseFilePath=None):
		"""
		Adds a step which sends a http call
		Params:
			testName - name of the test
			requestFilePath - file path containing http request
			responseFilePath - file path containing http response to test
		"""
		self.addTest(
			HttpTestStep(
				testName,
				url,
				requestFilePath,
				responseFilePath
			)
		)

	def addWait(self,testName,time):
		"""
		Adds a step which waits a specified number of seconds
		Params:
			testName - name of the test
			time - time to wait (s)
		"""
		self.addTest(WaitTestStep(testName,time))

	def addTest(self,test):
		"""
		Adds a test
		Params:
			test - test to add
		"""
		self.tests.append(test)

	def execute(self):
		"""
		Executes all tests in order of added
		"""
		for test in self.tests:
			#print("\n================================",flush=True)
			#print("\nRunning Test: "+test.testName,flush=True)
			test.execute()

"""
TestBuilder uses test steps which are executable
"""
class TestStep(unittest.TestCase):

	def __init__(self,testName=""):
		super().__init__()
		self.testName = testName

	def execute(self):
		"""
		Default method for being executed, this is for being overridden
		"""
		pass

"""
WaitTestStep involves sleeping for a designated time, this is useful for testing endpoints which have a bit of processing before completion
"""
class WaitTestStep(TestStep):

	def __init__(self,testName,time):
		super().__init__(testName)
		self.time = time

	def execute(self):
		"""
		Executes by waiting a designated time
		"""
		time.sleep(self.time)

"""
HttpTestStep sends a http request and tests the response matches expectations
"""
class HttpTestStep(TestStep):

	def __init__(self,testName,url,requestFilePath,responseFilePath=None):
		super().__init__(testName)
		self.requestFilePath=requestFilePath
		self.request=RequestMessage(url=url,filePath=requestFilePath)
		self.expectedResponse=ResponseMessage(url=url,filePath=responseFilePath) if responseFilePath else None

	def addDynamicHeader(self, headerName, headerValue):
		"""
		Used for adding headers which can't be defined statically in the file
		I.E adding JWTs which rely on dynamically generated keys
		Params:
			headerName name of header to add
			headerValue value of header to add
		Returns:
			self for method chaining
		"""
		self.request.addHeader(headerName,headerValue)
		return self

	def checkResponse(self,actualResponse):
		"""
		Runs unit tests on the expecte response and actual response
		Params:
			actualResponse - the actual response to be tested
		"""
		case=unittest.TestCase()
		#Sets ignored fields
		if self.expectedResponse.getHeaders():
			actualResponse.setIgnoredHeaderFields(self.expectedResponse.getIgnoredHeaderFields())
		if self.expectedResponse.getBody():
			actualResponse.setIgnoredBodyFields(self.expectedResponse.getIgnoredBodyFields())
		#Runs checks on fields
		#print("\nExpected Status Code: "+str(self.expectedResponse.getStatusCode()),flush=True)
		#print("Actual Status Code: "+str(actualResponse.getStatusCode()),flush=True)
		#print("\nExpected Reason: "+ str(self.expectedResponse.getReason()),flush=True)
		#print("Actual Reason: "+ str(actualResponse.getReason()),flush=True)
		#print("\nExpected Headers: " + str(self.expectedResponse.getHeaders()),flush=True)
		#print("Actual Headers: " + str(actualResponse.getHeaders()),flush=True)
		#print("\nExpected Body: "+ str(self.expectedResponse.getBody()),flush=True)
		#print("Actual Body: "+ str(actualResponse.getBody()),flush=True)

		self.assertEqual(self.expectedResponse.getStatusCode(),actualResponse.getStatusCode())


		self.assertEqual(self.expectedResponse.getReason(),actualResponse.getReason())


		self.assertDictEqual(dict(self.expectedResponse.getHeaders()),dict(actualResponse.getHeaders()))

		try:
			self.assertDictEqual(dict(self.expectedResponse.getBody()),dict(actualResponse.getBody()))
		except ValueError:
			self.assertListEqual(list(self.expectedResponse.getBody()),list(actualResponse.getBody()))

	def execute(self):
		"""
		Executes the Http send and validates the response if an expected response is set
		"""
		#print("Test File: "+self.requestFilePath,flush=True)
		actualResponse=HttpCall(self.request).send()
		if not actualResponse:
			self.fail("Failed decoding response")
		if self.expectedResponse:
			self.checkResponse(actualResponse)

"""
Inteface for handling http calls and generating the response message object
"""
class HttpCall():

	def __init__(self,request):
		self.request=request

	def send(self):
		"""
		Executes the HTTP call and returns as a response message
		"""
		url=self.request.getUrlString()
		headers=self.request.getHeaders()
		if self.request.getMethod() == "GET":
			response=requests.get(url, headers=headers)
		if self.request.getMethod() == "PUT":
			body=self.request.getBody()
			response=requests.put(url, headers=headers)
		if self.request.getMethod() == "DELETE":
			response=requests.remove(url, headers=headers)
		if self.request.getMethod() == "POST":
			body=self.request.getBody()
			response=requests.post(url, headers=headers, data=body)

		try:
			return ResponseMessage(
				url=url,
				headers=dict(response.headers),
				body=response.text,
				statusCode=response.status_code,
				reason=response.reason
			)
		except json.decoder.JSONDecodeError as e:
			#print("Failed decoding response as json",flush=True)
			#print("Response: "+response.text)
			return None


