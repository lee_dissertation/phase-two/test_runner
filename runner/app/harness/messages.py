import os
import json

class Url():

	def __init__(self,domain,isHttps=False,port=None,path=None):
		self.setDomain(domain)
		self.setIsHttps(isHttps)
		self.setPort(port)
		self.setPath(path)

	def isHttps(self):
		"""
		Returns true if https is enabled
		"""
		return self.isHttps

	def setIsHttps(self,isHttps):
		"""
		Sets the https value and updates the resource type 
		Params:
			isHttps - true if the url uses https
		"""
		self.isHttps=isHttps
		self.setResourceType("https://" if isHttps else "http://")

	def setResourceType(self,resourceType):
		"""
		Sets the resource type
		Params:
			resourceType - typically "https://" or "http://"
		"""
		self.resourceType = resourceType

	def getUrl(self):
		"""
		Generates the url using the classes attributes
		"""
		url = self.resourceType + self.getDomain()
		port = self.getPort()
		if port:
			url+= ":"+port
		path = self.getPath()
		if path:
			url+= path
		return url

	def getDomain(self):
		"""
		Returns the domain
		"""
		return self.domain

	def setDomain(self,domain):
		"""
		Sets the domain
		Params:
			domain - domain to set
		"""
		self.domain=domain

	def getPort(self):
		"""
		Gets the port
		"""
		return self.port

	def setPort(self,port):
		"""
		Sets the port, sets it as None to avoid crashing on the case of it assigning str(None)
		Params:
			port - port to set
		"""
		self.port=str(port) if port else None

	def getPath(self):
		"""
		Gets the path
		"""
		return self.path

	def setPath(self,path):
		"""
		Sets the path
		Params:
			path - path to set
		"""
		self.path=path

class HttpMessage():

	def __init__(self,url,protocol=None,headers=None,body=None,filePath=None):
		self.setUrl(url)
		self.setProtocol(protocol)
		self.setHeaders(headers)
		self.setBody(body)
		if filePath: self.fromFilePath(filePath)

	def getUrl(self):
		"""
		Gets the URL
		"""
		return self.url

	def getUrlString(self):
		"""
		Gets the URL as a string
		"""
		return self.url.getUrl()

	def setUrl(self,url):
		"""
		Sets the URL 
		Params:
			url - url to set
		"""
		self.url = url

	def getProtocol(self):
		"""
		Gets the protocol
		"""
		return self.protocol

	def setProtocol(self,protocol):
		"""
		Sets the protocol
		Params:
			protocol - protocol to set
		"""
		self.protocol=protocol

	def getHeaders(self):
		"""
		Gets the headers
		"""
		return self.headers

	def setHeaders(self,headers):
		"""
		Sets the headers, assigns an empty dictionery if None
		Params:
			headers - headers  to set
		"""
		self.headers = headers or {}

	def getHeader(self,key):
		"""
		Gets a header from key
		Params:
			Key - key to get
		"""
		return self.headers[key]

	def addHeader(self,key,value):
		"""
		Adds a header
		Params:
			key - key to add
			value - value to add
		"""
		self.headers[key]=value

	def setFirstLine(self,line):
		"""
		Sets the first line, for a http message this is the protocol
		Params:
			line - line to parse
		"""
		self.setProtocol(line)

	def addHeaderFromLine(self,line):
		"""
		Reads a header line and parses into the headers dictionary, example line is "header1: value1"
		Param:
			line - line to parse
		"""
		line=line.split(":")
		headerKey=line[0].strip()
		headerVal=line[1].strip()
		self.addHeader(headerKey,headerVal)

	def getBody(self):
		"""
		Gets the body
		"""
		return self.body

	def setBody(self,body):
		"""
		Sets the body
		Params:
			body - body to set
		"""
		self.body = body

	def fromFilePath(self,filePath):
		"""
		Parses a whole file using setFirstLine and addHeaderFromLine to generate headers and protocol
		Params:
			filePath - path of the file to parse
		"""
		with open(filePath.replace('\\','/'),'r') as file_contents:
			isFirstLine=True
			isHead=False
			body=""
			for line in file_contents:
				if isFirstLine:
					self.setFirstLine(line)
					isFirstLine=False
					isHead=True
					continue
				if isHead and line is "\n":
					isHead = False
					continue
				if isHead:
					self.addHeaderFromLine(line)
					continue
				body+=line

			if body:
				self.setBody(body)

class JsonMessage(HttpMessage):

	def __init__(self,url,protocol=None,headers=None,body=None,filePath=None):
		super().__init__(url=url,protocol=protocol,headers=headers,body=body,filePath=filePath)

	def setBody(self,body):
		"""
		Sets the body, parses into a dict if not already
		Params:
			body - body to set
		"""
		if isinstance(body,str):
			self.body=json.loads(body.replace("'", '"'))

		if isinstance(body,dict):
			self.body=body

	def getIgnoredBodyFields(self):
		"""
		Returns the keys for all fields with the value (ignore) in the body
		"""
		return self.findIgnoredFields(self.getBody(),[])

	def getIgnoredHeaderFields(self):
		"""
		Returns the keys for all fields with the value (ignore) in the headers
		"""
		return self.findIgnoredFields(self.getHeaders(),[])

	def findIgnoredFields(self,jsonNode,ignoredFields):
		"""
		Recursively iterates the node to find fields with the value (ignore) and adds it to the ignoredFields
		Params:
			jsonNode - current node in dictionary to parse
			ignoredFields - array featuring all fields matching value (ignore)
		"""

		if isinstance(jsonNode,dict):
			for key, val in jsonNode.items():
				if isinstance(val,str):
					if val == "(ignore)":
						ignoredFields.append(key)
					continue
				self.findIgnoredFields(val,ignoredFields)

		if isinstance(jsonNode, list):
			for v in jsonNode:
				self.findIgnoredFields(v,ignoredFields)

		return ignoredFields


	def setIgnoredBodyFields(self,fieldsToIgnore):
		"""
		Sets the value of all fieldsToIgnore as (ignore) to body
		Params:
			fieldsToIgnore - array containing the keys to all fields that want updated
		"""
		body = self.getBody()
		if body:
			self.setBody(self.addIgnoredFields(body,fieldsToIgnore))

	def setIgnoredHeaderFields(self,fieldsToIgnore):
		"""
		Sets the value of all fieldsToIgnore as (ignore) to headers
		Params:
			fieldsToIgnore - array containing the keys to all fields that want updated
		"""
		headers = self.getHeaders()
		if headers:
			self.setHeaders(self.addIgnoredFields(headers,fieldsToIgnore))

	def addIgnoredFields(self,jsonNode,fieldsToIgnore):
		"""
		Recursively searches the jsonNode to set the value of any key in fieldsToIgnore to (ignore)
		Params:
			jsonNode - current node in dictionary to parse
			ignoredFields - array featuring all fields matching value (ignore)
		"""
		if isinstance(jsonNode,dict):
			for key, val in list(jsonNode.items()):
				if key in fieldsToIgnore:
					jsonNode[key] = "(ignore)"
					continue
				if isinstance(val,dict):
					jsonNode[key]=self.addIgnoredFields(val,fieldsToIgnore)
					continue
				if isinstance(val, list):
					for i,v in enumerate(val):
						jsonNode[key][i]=self.addIgnoredFields(v,fieldsToIgnore)
					continue
				if isinstance(val,str):
					continue
		return jsonNode

class ResponseMessage(JsonMessage):

	def __init__(self,url,protocol=None,headers=None,body=None,filePath=None,statusCode=None,reason=None):
		self.setStatusCode(statusCode)
		self.setReason(reason)
		super().__init__(url=url,protocol=protocol,headers=headers,body=body,filePath=filePath)

	def setFirstLine(self,firstLine):
		"""
		Sets the first line, overrides the HttpMessage method
		Params:
			firstLine - the first line in a txt file to parse
		"""
		line=[l.strip() for l in firstLine.split(" ",2)]
		self.setProtocol(line[0])
		self.setStatusCode(int(line[1]))
		self.setReason(line[2])

	def setStatusCode(self,statusCode):
		"""
		Sets the status code
		Params:
			statusCode - status code to set
		"""
		self.statusCode=statusCode

	def getStatusCode(self):
		"""
		Gets the status code
		"""
		return self.statusCode

	def setReason(self,reason):
		"""
		Sets the reason
		Params:
			reason - reason to set
		"""
		self.reason=reason

	def getReason(self):
		"""
		Gets the reason
		"""
		return self.reason

class RequestMessage(JsonMessage):

	def __init__(self,url,protocol=None,headers=None,body=None,filePath=None,method=None,path=None):
		super().__init__(url=url,protocol=protocol,headers=headers,body=body,filePath=filePath)

	def setFirstLine(self,firstLine):
		"""
		Sets the first line, overrides the HttpMessage method
		Params:
			firstLine - the first line in a txt file to parse
		"""
		line=[l.strip() for l in firstLine.split(" ")]
		self.setMethod(line[0])
		self.setPath(line[1])
		self.setProtocol(line[2])

	def setMethod(self,method):
		"""
		Sets the method
		Params:
			method - method to set
		"""
		self.method=method

	def getMethod(self):
		"""
		Gets the method
		"""
		return self.method

	def setPath(self,path):
		"""
		Sets the path
		Params:
			path - path to set
		"""
		self.getUrl().setPath(path)

	def getPath(self):
		"""
		Gets the path
		"""
		return self.getUrl().getPath()
