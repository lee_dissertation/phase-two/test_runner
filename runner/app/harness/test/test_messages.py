import unittest,sys,os
sys.path.insert(1, os.path.join(sys.path[0], '..'))

import messages

def thisDir(filePath):
	"""
	Ensures that this script can be run from any directory without damaging the relative file paths 
	Params:
		filePath - path relative to this files directory
	"""
	return os.path.dirname(os.path.realpath(__file__))+filePath

class TestUrl(unittest.TestCase):

	def testUrl(self):
		"""
		Tests a default url generated from a domain
		"""
		url=messages.Url("lerring.co.uk")
		self.assertEqual("http://lerring.co.uk",url.getUrl())

	def testUrlHttps(self):
		"""
		Tests a default url generated from a domain with the https flag
		"""
		url=messages.Url("lerring.co.uk",isHttps=True)
		self.assertEqual("https://lerring.co.uk",url.getUrl())

	def testUrlPort(self):
		"""
		Tests a default url generated from a domain with a port
		"""
		url=messages.Url("lerring.co.uk",port=8080)
		self.assertEqual("http://lerring.co.uk:8080",url.getUrl())

	def testUrlPath(self):
		"""
		Tests a default url generated from a domain with a path
		"""
		url=messages.Url("lerring.co.uk",path="/path_1/path_2")
		self.assertEqual("http://lerring.co.uk/path_1/path_2",url.getUrl())

class TestHttpMessage(unittest.TestCase):

	def testHttpMessage(self):
		"""
		Tests generating a url string 
		"""
		url=messages.Url("lerring.co.uk")
		message = messages.HttpMessage(url=url)
		self.assertEqual(url.getUrl(),message.getUrlString())

	def testHttpMessageHeaders(self):
		"""
		Tests headers are set correctly
		"""
		url=messages.Url("lerring.co.uk")
		message = messages.HttpMessage(url=url,headers={'key':'value'})
		self.assertEqual(url.getUrl(),message.getUrlString())
		self.assertDictEqual({'key':'value'},message.getHeaders())

	def testHttpMessageAddHeaderFromLine(self):
		"""
		Tests headers derived from a single line are parsed correctly
		"""
		url=messages.Url("lerring.co.uk")
		message = messages.HttpMessage(url=url)
		message.addHeaderFromLine("key:value")
		self.assertEqual(url.getUrl(),message.getUrlString())
		self.assertDictEqual({'key':'value'},message.getHeaders())

	def testHttpMessageAddHeader(self):
		"""
		Tests adding a header manually
		"""
		url=messages.Url("lerring.co.uk")
		message = messages.HttpMessage(url=url)
		message.addHeader("key","value")
		self.assertEqual(url.getUrl(),message.getUrlString())
		self.assertDictEqual({'key':'value'},message.getHeaders())

	def testHttpMessageFile(self):
		"""
		Tests reading a http message from a file
		"""
		url=messages.Url("lerring.co.uk")
		message = messages.HttpMessage(url=url,filePath=thisDir("/data/http_message.txt"))
		self.assertEqual(url.getUrl(),message.getUrlString())
		self.assertEqual('http\n',message.getProtocol())
		self.assertDictEqual({'header1':'value','header2':'value'},message.getHeaders())
		self.assertEqual('body',message.getBody())

class TestJsonMessage(unittest.TestCase):

	def testJsonMessage(self):
		"""
		Tests generating a json message and comparing the bodies
		"""
		url=messages.Url("lerring.co.uk")
		json_message=messages.JsonMessage(url)
		body={
			'key1':'value1'
		}
		json_message.setBody(body)
		self.assertDictEqual(body,json_message.getBody())

	def testJsonMessageFile(self):
		"""
		Tests generating a json message from a .txt file
		"""
		url=messages.Url("lerring.co.uk")
		json_message=messages.JsonMessage(url,filePath=thisDir("/data/json_message.txt"))
		body={
			'key1':'value1'
		}
		self.assertDictEqual(body,json_message.getBody())

	def testJsonMessageGetIgnore(self):
		"""
		Tests generating a json message from a .txt file and finding ignored fields
		"""
		url=messages.Url("lerring.co.uk")
		json_message=messages.JsonMessage(url,filePath=thisDir("/data/json_message_ignore.txt"))
		self.assertEqual(['header2'],json_message.getIgnoredHeaderFields())
		self.assertEqual(['key1'],json_message.getIgnoredBodyFields())

	def testJsonMessageSetIgnore(self):
		"""
		Tests setting ignored fields
		"""
		url=messages.Url("lerring.co.uk")
		json_message=messages.JsonMessage(url)
		body={
			'key1':'value1'
		}
		json_message.setBody(body)
		json_message.setIgnoredBodyFields('key1')
		ignored={
			'key1':'(ignore)'
		}
		self.assertDictEqual(ignored,json_message.getBody())

class TestResponseMessage(unittest.TestCase):

	def testResponseMessage(self):
		"""
		Tests generating a response message from a file
		"""
		url=messages.Url("lerring.co.uk")
		res_message=messages.ResponseMessage(url,filePath=thisDir("/data/response_message.txt"))
		self.assertEqual("protocol",res_message.getProtocol())
		self.assertEqual(200,res_message.getStatusCode())
		self.assertEqual("reason",res_message.getReason())

class TestRequestMessage(unittest.TestCase):

	def testRequestMessage(self):
		"""
		Tests generating a request message from a file
		"""
		url=messages.Url("lerring.co.uk")
		req_message=messages.RequestMessage(url,filePath=thisDir("/data/request_message.txt"))
		self.assertEqual("method",req_message.getMethod())
		self.assertEqual("path",req_message.getPath())
		self.assertEqual("protocol",req_message.getProtocol())
