import unittest
#import socket
#import sys
from copy import deepcopy

from harness.builder import *
from lib.lerring_conf import LerringConf
from lib.jwt_helpers import SelfSignedJwtHS256

"""
Creates all of the possible test steps that will be used to interact with the API
"""
class AppTest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		cls.url=Url(
			domain="localhost",
			port="8080"
		)
		conf=LerringConf('/lerring/lerring.conf')
		cls.jwt=SelfSignedJwtHS256(
			conf.getUuid()
		).createToken(payload={'permissions':['apps','repos']},headers={'typ':'JWT'})
		test = TestBuilder()
		#clones if not already
		test.addTest(cls.pendingClone(expect=False))
		test.addWait(testName="Wait 5",time=5)
		#bring to "built state" if already deployed
		test.addTest(cls.pendingStop(expect=False))
		test.addWait(testName="Wait 15",time=15)
		#bring to "build state" if not built"
		test.addTest(cls.pendingBuild())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(cls.completeBuild())
		test.addWait(testName="Wait 5",time=5)
		test.execute()

	@classmethod
	def tearDownClass(cls):
		test = TestBuilder()
		test.addTest(cls.pendingStop(expect=False))
		test.addWait(testName="Wait 10",time=10)
		test.addTest(cls.pendingRemoveRepo(expect=False))
		test.addWait(testName="Wait 5",time=5)
		test.execute()

	@classmethod
	def pendingBuild(cls, expect=True):
        """
        Creates a test step for a build app request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Build Pending",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/build/get_build_pending_request.txt",
			responseFilePath="data/apps_api/build/get_build_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def completeBuild(cls, expect=True):
       """
        Following a build request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Build Complete",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/build/get_build_complete_request.txt",
			responseFilePath="data/apps_api/build/get_build_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def pendingDeploy(cls, expect=True):
        """
        Creates a test step for a deploy app request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Deploy Pending",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/deploy/get_deploy_pending_request.txt",
			responseFilePath="data/apps_api/deploy/get_deploy_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def completeDeploy(cls, expect=True):
       """
        Following a deploy request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Deploy Complete",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/deploy/get_deploy_complete_request.txt",
			responseFilePath="data/apps_api/deploy/get_deploy_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def pendingExec(cls, expect=True):
        """
        Creates a test step for a exec command request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Execute Pending",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/exec/get_exec_pending_request.txt",
			responseFilePath="data/apps_api/exec/get_exec_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def completeExec(cls, expect=True):
       """
        Following a execute command request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Execute Complete",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/exec/get_exec_complete_request.txt",
			responseFilePath="data/apps_api/exec/get_exec_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def pendingStop(cls, expect=True):
        """
        Creates a test step for a stop app request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Stop Pending",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/stop/get_stop_pending_request.txt",
			responseFilePath="data/apps_api/stop/get_stop_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def completeStop(cls, expect=True):
       """
        Following a stop request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Stop Pending",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/stop/get_stop_complete_request.txt",
			responseFilePath="data/apps_api/stop/get_stop_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def completeApps(cls, expect=True):
       """
        Following a get apps request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Apps Complete",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/apps/get_apps_request.txt",
			responseFilePath="data/apps_api/apps/get_apps_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def completeStatus(cls, expect=True):
       """
        Following a get status request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Get Status",
			url=deepcopy(cls.url),
			requestFilePath="data/apps_api/status/get_status_request.txt",
			responseFilePath="data/apps_api/status/get_status_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	#
	# Repo calls
	#

	@classmethod
	def pendingRemoveRepo(cls,expect=True):
        """
        Creates a test step for a remove repo request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Remove Pending",
			url=deepcopy(cls.url),
			requestFilePath="data/repos_api/remove/get_remove_request.txt",
			responseFilePath="data/repos_api/remove/get_remove_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def completeRemoveRepo(cls,expect=True):
        """
        Following a remove request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Remove Complete",
			url=deepcopy(cls.url),
			requestFilePath="data/repos_api/repos/get_repos_request.txt",
			responseFilePath="data/repos_api/remove/get_remove_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)


	@classmethod
	def pendingClone(cls, expect=True):
        """
        Creates a test step for clone repo request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Clone Pending",
			url=deepcopy(cls.url),
 			requestFilePath="data/repos_api/clone/get_clone_request.txt",
 			responseFilePath="data/repos_api/clone/get_clone_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def completeClone(cls, expect=True):
        """
        Following a clone request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Clone Complete",
			url=deepcopy(cls.url),
			requestFilePath="data/repos_api/repos/get_repos_request.txt",
	 		responseFilePath="data/repos_api/clone/get_clone_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

"""
Tests the /deploy endpoint
"""
class TestDeploy(AppTest):

	def testDeploy(self):
		"""
		Tests deploying an application, validating it is deployed
		"""
		test = TestBuilder()
		test.addTest(self.pendingDeploy())
		test.addWait(testName="Wait 15",time=15)
		test.addTest(self.completeDeploy())
		test.execute()

	def testDeployNotFound(self):
		"""
		Tests deploying an application which doesn't exist
		"""
		test = TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="Deploy Not Found",
				url=deepcopy(self.url),
				requestFilePath="data/apps_api/deploy/get_deploy_nf_request.txt",
				responseFilePath="data/apps_api/deploy/get_deploy_nf_response.txt"
			).addDynamicHeader('Authorization',self.jwt)
		)
		test.execute()

"""
Tests the /stop endpoint
"""
class TestStop(AppTest):

	def setUp(self):
		"""
		Deploys the application ready to be stopped
		"""
		test = TestBuilder()
		test.addTest(self.pendingDeploy(expect=False))
		test.addWait(testName="Wait 15",time=15)
		test.execute()

	def testStop(self):
		"""
		Tests stopping an application, then validating it is stopped
		"""
		test=TestBuilder()
		test.addTest(self.pendingStop())
		test.addWait(testName="Wait 15",time=15)
		test.addTest(self.completeStop())
		test.execute()

	def testStopNotFound(self):
		"""
		Tests stopping an application that doesn't exist
		"""
		test = TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="Stop Not Found",
				url=deepcopy(self.url),
				requestFilePath="data/apps_api/stop/get_stop_nf_request.txt",
				responseFilePath="data/apps_api/stop/get_stop_nf_response.txt"
			).addDynamicHeader('Authorization',self.jwt)
		)
		test.execute()

	def testStopWrongState(self):
		"""
		Tests stopping an application that isn't running
		"""
		test=TestBuilder()
		#put app in stopped state regardless
		test.addTest(self.pendingStop(expect=False))
		test.addWait(testName="Wait 15",time=15)
		#try stopping again.. not possible
		test.addTest(
			HttpTestStep(
				testName="Stop Wrong State",
				url=deepcopy(self.url),
				requestFilePath="data/apps_api/stop/get_stop_pending_request.txt",
				responseFilePath="data/apps_api/stop/get_stop_ws_response.txt"
			).addDynamicHeader('Authorization',self.jwt)
		)
		test.execute()

"""
Tests the /status endpoint
"""
class TestStatus(AppTest):

	def testStatus(self):
		"""
		Tests getting the status of an app
		"""
		test=TestBuilder()
		test.addTest(self.completeStatus())
		test.execute()

	def testStatusNotFound(self):
		"""
		Tests getting the status of an app that doesn't exist
		"""
		test=TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="Status Not Found",
				url=deepcopy(self.url),
				requestFilePath="data/apps_api/status/get_status_nf_request.txt",
				responseFilePath="data/apps_api/status/get_status_nf_response.txt"
			).addDynamicHeader('Authorization',self.jwt)
		)
		test.execute()


"""
Tests the /app endpoint
"""
class TestApp(AppTest):

	def testApp(self):
		"""
		Tests getting the test_repo app, showing info from the docker-compose file
		"""
		test=TestBuilder()
		test.addTest(self.completeApps())
		test.execute()

	def testAppNotFound(self):
		"""
		Tests getting the an app which doesn't exist
		"""
		test=TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="App Not Found",
				url=deepcopy(self.url),
				requestFilePath="data/apps_api/apps/get_apps_nf_request.txt",
				responseFilePath="data/apps_api/apps/get_apps_nf_response.txt"
			).addDynamicHeader('Authorization',self.jwt)
		)
		test.execute()

"""
Tests the /exec endpoint
"""
class TestExec(AppTest):

	def testExec(self):
		"""
		Tests executing a command on the running container, verifying it is complete
		"""
		test=TestBuilder()
		test.addTest(self.pendingExec())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeExec())
		test.execute()

	def testExecNotFound(self):
		"""
		Tests executing on an app which doesn't exist
		"""
		test=TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="App Not Found",
				url=deepcopy(self.url),
				requestFilePath="data/apps_api/exec/get_exec_nf_request.txt",
				responseFilePath="data/apps_api/exec/get_exec_nf_response.txt"
			).addDynamicHeader('Authorization',self.jwt)
		)
		test.execute()

"""
Tests running a monolithic test containing a full transaction
"""
class TestFullTransaction(AppTest):


	def step_1_GetApps(self):
		"""
		First step, getting apps
		"""
		test = TestBuilder()
		test.addTest(self.completeApps())
		test.execute()

	def step_2_GetStatus(self):
		"""
		Second step, getting app status
		"""
		test = TestBuilder()
		test.addTest(self.completeStatus())
		test.execute()

	def step_3_GetDeploy(self):
		"""
		Third step, deploying the application
		"""
		test = TestBuilder()
		test.addTest(self.pendingDeploy())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeDeploy())
		test.execute()

	def step_4_GetExec(self):
		"""
		Fourth step, executing a command on the application
		"""
		test = TestBuilder()
		test.addTest(self.pendingExec())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeExec())
		test.execute()

	def step_5_GetStop(self):
		"""
		Fifth step, stopping application
		"""
		test = TestBuilder()
		test.addTest(self.pendingStop())
		test.addWait(testName="Wait 10",time=10)
		test.addTest(self.completeStop())
		test.execute()

	def _steps(self):
		for name in dir(self): # dir() result is implicitly sorted
			if name.startswith("step"):
				yield name, getattr(self, name)

	def test_steps(self):
		for name, step in self._steps():
			try:
				step()
			except Exception as e:
				self.fail("{} failed ({}: {})".format(step, type(e), e))

