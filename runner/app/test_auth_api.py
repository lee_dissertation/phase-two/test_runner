import unittest
from copy import deepcopy

from harness.builder import *
from lib.lerring_conf import LerringConf
from lib.jwt_helpers import SelfSignedJwtHS256
from lib.networking import Networking

class AuthTest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		cls.url=Url(
			domain="localhost",
			port="8080"
		)
		conf=LerringConf('/lerring/lerring.conf')
		cls.uuid=conf.getUuid()
		cls.jwt=SelfSignedJwtHS256(
			cls.uuid
		).createToken(payload={'permissions':['apps','repos']},headers={'typ':'JWT'})
		cls.mac=Networking.getMac()

	@classmethod
	def getJwt(cls, expect=True):
		"""
		Tests getting a key signed by hosts private key
		Expects jwt signed by host uuid OR jwt signed
		by a nodes private key that matches its public key in the key_db
		"""
		return HttpTestStep(
			testName="Get JWT",
			url=deepcopy(cls.url),
			requestFilePath="data/auth_api/get_auth_request.txt",
			responseFilePath="data/auth_api/get_auth_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

	@classmethod
	def getTest(cls, expect=True):
		"""
		Uses the /test url to validate the authorization
		middleware works as expected
		"""
		return HttpTestStep(
			testName="Get Test",
			url=deepcopy(cls.url),
			requestFilePath="data/auth_api/get_test_request.txt",
			responseFilePath="data/auth_api/get_test_response.txt" if expect else None
		).addDynamicHeader('Authorization',cls.jwt)

"""
Tests a nodes authorisation endpoint
"""
class TestAuth(AuthTest):

	def testGetAuth(self):
        """
        Tests getting a JWT from the authorisation endpoint
        """
		test=TestBuilder()
		test.addTest(self.getJwt().addDynamicHeader('Mac-Addr',self.mac))
		test.execute()

	def testGetAuthBadMac(self):
        """
        Tests getting a JWT from the authorisation using a mac address incorrectly assigned to the requesting JWT
        """
		test=TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="Get JWT bad mac",
				url=deepcopy(self.url),
				requestFilePath="data/auth_api/get_auth_request.txt",
				responseFilePath="data/auth_api/get_auth_bad_mac_response.txt"
			).addDynamicHeader('Authorization',self.jwt).addDynamicHeader('Mac-Addr','bad_mac')
		)
		test.execute()

	def testGetAuthNoMac(self):
        """
        Tests getting a JWT with no mac in the request
        """
		test=TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="Get JWT no mac",
				url=deepcopy(self.url),
				requestFilePath="data/auth_api/get_auth_request.txt",
				responseFilePath="data/auth_api/get_auth_no_mac_response.txt"
			).addDynamicHeader('Authorization',self.jwt)
		)
		test.execute()


	def testGetAuthBadSig(self):
        """
        Tests getting a JWT with a faked signature
        """
		test=TestBuilder()
		badSig=SelfSignedJwtHS256(
			'this_sig_is_wrong'
		).createToken(payload={'permissions':['apps','repos']},headers={'typ':'JWT'})
		test.addTest(
			HttpTestStep(
				testName="Get JWT bad sig",
				url=deepcopy(self.url),
				requestFilePath="data/auth_api/get_auth_request.txt",
				responseFilePath="data/auth_api/get_auth_bad_sig_response.txt"
			).addDynamicHeader('Authorization',badSig).addDynamicHeader('Mac-Addr',self.mac)
		)
		test.execute()

	def testGetAuthNoJwt(self):
        """
        Tests getting a JWT with no request JWT specified
        """
		test=TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="Get JWT no jwt",
				url=deepcopy(self.url),
				requestFilePath="data/auth_api/get_auth_request.txt",
				responseFilePath="data/auth_api/get_auth_no_jwt_response.txt"
			).addDynamicHeader('Mac-Addr',self.mac)
		)
		test.execute()

	def testGetAuthManInTheMiddle(self):
        """
        Tests getting a JWT with a modified request jwt
        """
		test=TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="Get JWT mitm",
				url=deepcopy(self.url),
				requestFilePath="data/auth_api/get_auth_request.txt",
				responseFilePath="data/auth_api/get_auth_mitm_response.txt"
			).addDynamicHeader('Authorization',"substring"+self.jwt).addDynamicHeader('Mac-Addr',self.mac)
		)
		test.execute()

"""
Tests the middleware used between APIs and the requester
"""
class TestAuthMiddleware(AuthTest):

	def testGetTest(self):
        """
        Tests the middleware success 
        """
		test=TestBuilder()
		test.addTest(self.getTest())
		test.execute()

	def testGetTestNoJwt(self):
        """
        Tests the middleware with no JWT in request 
        """
		test=TestBuilder()
		test.addTest(
			HttpTestStep(
				testName="Get Test No JWT",
				url=deepcopy(self.url),
				requestFilePath="data/auth_api/get_test_request.txt",
				responseFilePath="data/auth_api/get_test_no_jwt_response.txt"
			)
		)
		test.execute()

	def testGetTestNoPermissions(self):
        """
        Tests the middleware without sufficient permissions for the API 
        """
		test=TestBuilder()
		badSig=SelfSignedJwtHS256(
			self.uuid
		).createToken(payload={'permissions':[]},headers={'typ':'JWT'})
		test.addTest(
			HttpTestStep(
				testName="Get Test No Permissions",
				url=deepcopy(self.url),
				requestFilePath="data/auth_api/get_test_request.txt",
				responseFilePath="data/auth_api/get_test_no_permissions_response.txt"
			).addDynamicHeader('Authorization',badSig)
		)
		test.execute()

	def testGetTestBadSig(self):
        """
        Tests the middleware with a signature from a bad actor 
        """
		test=TestBuilder()
		badSig=SelfSignedJwtHS256(
			'this_sig_is_wrong'
		).createToken(payload={'permissions':['apps','repos']},headers={'typ':'JWT'})
		test.addTest(
			HttpTestStep(
				testName="Get Test Bad SIgnature",
				url=deepcopy(self.url),
				requestFilePath="data/auth_api/get_test_request.txt",
				responseFilePath="data/auth_api/get_test_bad_sig_response.txt"
			).addDynamicHeader('Authorization',badSig)
		)
		test.execute()
