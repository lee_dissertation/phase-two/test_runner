import unittest
#import socket
#import sys
from copy import deepcopy

from harness.builder import *
from lib.lerring_conf import LerringConf
from lib.jwt_helpers import SelfSignedJwtHS256

"""
Creates all of the possible test steps that will be used to interact with the API
"""
class RepoTest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		cls.url=Url(
			domain="localhost",
			port="8080"
		)
		conf=LerringConf('/lerring/lerring.conf')
		cls.jwt=SelfSignedJwtHS256(
			conf.getUuid()
		).createToken(payload={'permissions':['repos']},headers={'typ':'JWT'})

	def pendingRemove(self,expect=True):
        """
        Creates a test step for a remove repo request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Remove Pending",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/remove/get_remove_request.txt",
			responseFilePath="data/repos_api/remove/get_remove_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',self.jwt)

	def completeRemove(self,expect=True):
        """
        Following a remove request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Remove Complete",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/repos/get_repos_request.txt",
			responseFilePath="data/repos_api/remove/get_remove_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',self.jwt)

	def pendingPull(self, expect=True):
        """
        Creates a test step for a pull repo request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Pull Pending",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/pull/get_pull_request.txt",
			responseFilePath="data/repos_api/pull/get_pull_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',self.jwt)

	def completePull(self, expect=True):
        """
        Following a pull request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Pull Complete",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/repos/get_repos_request.txt",
			responseFilePath="data/repos_api/pull/get_pull_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',self.jwt)

	def pendingClone(self, expect=True):
        """
        Creates a test step for clone repo request
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Clone Pending",
			url=deepcopy(self.url),
 			requestFilePath="data/repos_api/clone/get_clone_request.txt",
 			responseFilePath="data/repos_api/clone/get_clone_pending_response.txt" if expect else None
		).addDynamicHeader('Authorization',self.jwt)

	def completeClone(self, expect=True):
        """
        Following a clone request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Clone Complete",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/repos/get_repos_request.txt",
	 		responseFilePath="data/repos_api/clone/get_clone_complete_response.txt" if expect else None
		).addDynamicHeader('Authorization',self.jwt)

	def completeRepos(self):
        """
        Following a get repos request this checks for it to be complete
        Params:
            expect - true this response is tested
        """
		return HttpTestStep(
			testName="Repos Complete",
			url=deepcopy(self.url),
 			requestFilePath="data/repos_api/repos/get_repos_request.txt",
			responseFilePath="data/repos_api/repos/get_repos_response.txt"
		).addDynamicHeader('Authorization',self.jwt)

class TestRemove(RepoTest):

	def setUp(self):
		test = TestBuilder()
		test.addTest(self.pendingClone(expect=False))
		test.addWait(testName="Wait 10",time=10)
		test.execute()

	def testRemoveRepo(self):
        """
        Tests removing a repo request
        """
		test = TestBuilder()
		test.addTest(self.pendingRemove())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeRemove())
		test.execute()

class TestPull(RepoTest):

	def setUp(self):
		test = TestBuilder()
		test.addTest(self.pendingClone(expect=False))
		test.addWait(testName="Wait 5",time=5)
		test.execute()

	def testGetPull(self):
        """
        Tests pulling a repo request
        """
		test = TestBuilder()
		test.addTest(self.pendingPull())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completePull())
		test.execute()

	def testGetPullNotFound(self):
        """
        Tests pulling a non existing repo request
        """
		test = TestBuilder()
		test.addSend(
			testName="Pull Not Found",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/pull/get_pull_not_found_request.txt",
			responseFilePath="data/repos_api/pull/get_pull_not_found_response.txt"
		)
		test.execute()

	def tearDown(self):
		test = TestBuilder()
		test.addTest(self.pendingRemove())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeRemove())
		test.execute()

class TestClone(RepoTest):

	def setUp(self):
		test = TestBuilder()
		test.addTest(self.pendingRemove(expect=False))
		test.addWait(testName="Wait 5",time=5)
		test.execute()

	def testGetClone(self):
        """
        Tests clone repo request
        """
		test = TestBuilder()
		test.addTest(self.pendingClone())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeClone())
		test.execute()

	def testGetCloneNotFound(self):
        """
        Tests cloning a non existing repo request
        """
		test = TestBuilder()
		test.addSend(
			testName="Clone Repo Not Found",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/clone/get_clone_not_found_request.txt",
			responseFilePath="data/repos_api/clone/get_clone_not_found_response.txt"
		)
		test.execute()

	def testGetCloneEmptyQuery(self):
        """
        Tests cloning a repo without specifying a repo
        """
		test = TestBuilder()
		test.addSend(
			testName="Clone Repo Empty",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/clone/get_clone_empty_query_request.txt",
			responseFilePath="data/repos_api/clone/get_clone_empty_query_response.txt"
		)
		test.execute()

	def tearDown(self):
		test = TestBuilder()
		test.addTest(self.pendingRemove(expect=False))
		test.addWait(testName="Wait 5",time=5)
		test.execute()

class TestRepos(RepoTest):

	def setUp(self):
		test = TestBuilder()
		test.addTest(self.pendingClone(expect=False))
		test.addWait(testName="Wait 5",time=5)
		test.execute()

	def testGetRepos(self):
        """
        Tests a GET request for repos
        """
		test = TestBuilder()
		test.addTest(self.completeRepos())
		test.execute()

	def testGetReposNotFound(self):
        """
        Tests a GET request for non existing repo
        """
		test = TestBuilder()
		test.addSend(
			testName="Get Repo Not Found",
			url=deepcopy(self.url),
			requestFilePath="data/repos_api/repos/get_repos_not_found_request.txt",
			responseFilePath="data/repos_api/repos/get_repos_not_found_response.txt"
		)
		test.execute()

	def tearDown(self):
		test = TestBuilder()
		test.addTest(self.pendingRemove())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeRemove())
		test.execute()

"""
Monolithic test for a full transaction
"""
class TestFullTransaction(RepoTest):

	def step_0_setup(self):
		test = TestBuilder()
		test.addTest(self.pendingRemove(expect=False))
		test.addWait(testName="Wait 10",time=10)
		test.execute()

	def step_1_GetClone(self):
		test = TestBuilder()
		test.addTest(self.pendingClone())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeClone())
		test.execute()

	def step_2_GetRepos(self):
		test = TestBuilder()
		test.addTest(self.completeRepos())
		test.execute()

	def step_3_GetPull(self):
		test = TestBuilder()
		test.addTest(self.pendingPull())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completePull())
		test.execute()

	def step_4_GetRemove(self):
		test = TestBuilder()
		test.addTest(self.pendingRemove())
		test.addWait(testName="Wait 5",time=5)
		test.addTest(self.completeRemove())
		test.execute()

	def _steps(self):
		for name in dir(self): # dir() result is implicitly sorted
			if name.startswith("step"):
				yield name, getattr(self, name)

	def test_steps(self):
		for name, step in self._steps():
			try:
				step()
			except Exception as e:
				self.fail("{} failed ({}: {})".format(step, type(e), e))

